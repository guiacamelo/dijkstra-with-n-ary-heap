#include <iostream>
#include <list>
#include <vector>
#include <string>
#include "vertex.h"
using namespace std;

typedef list<Vertex> ListAdj;

class Graph {
private:
	vector<ListAdj> graph;

public:
	
	Graph();
	


	ListAdj insertNeighbour(ListAdj ListAdj,int id, int distance);
	ListAdj newVertex(int id);
	void print();
	int graphSize(){
		return graph.size();
	}


	void addEdge(int origin, int destiny, int distance); 
	void insertVertex(ListAdj ListAdj);
	ListAdj* getNeighbourhood(int id);

	void loadGraph(string file);
};