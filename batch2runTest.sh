rm Testes/batch2/*.result

./dijkstra 469 796 Testes/batch2/1000 >> Testes/batch2/1000.result
./dijkstra 702 759 Testes/batch2/900 >> Testes/batch2/900.result
./dijkstra 737 550 Testes/batch2/910 >> Testes/batch2/910.result
./dijkstra 487 492 Testes/batch2/920 >> Testes/batch2/920.result
./dijkstra 885 225 Testes/batch2/930 >> Testes/batch2/930.result
./dijkstra 892 469 Testes/batch2/940 >> Testes/batch2/940.result
./dijkstra 146 409 Testes/batch2/950 >> Testes/batch2/950.result
./dijkstra 560 715 Testes/batch2/960 >> Testes/batch2/960.result
./dijkstra 398 86  Testes/batch2/970 >> Testes/batch2/970.result
./dijkstra 762 609 Testes/batch2/980 >> Testes/batch2/980.result
./dijkstra 180 141 Testes/batch2/990 >> Testes/batch2/990.result

for i in {900..1000..10}
do
    if cmp Testes/batch2/$i.dat Testes/batch2/$i.result; then
    	echo TRUE for $i
    else
    	echo FALSE for $i
	fi
	echo Testes/batch2/$i.dat Testes/batch2/$i.result
done