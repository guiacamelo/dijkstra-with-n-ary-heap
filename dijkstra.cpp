#include "heap.h"
#include "graph.h"
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <cassert>
#include <time.h>
#include <iostream>
#include <string> 
//#include <ctime>// include this header
//#include <chrono>
vector<int> dijkstra(Graph graph, int s, int t,std::ofstream& output)
{
	Heap q(7);
	int deletemin=0, incert=0, update=0;
	Vertex v(0,0), u(0,0);
	ListAdj *n;
	ListAdj::iterator it;
	vector<int> dist(graph.graphSize() + 1);
	vector<bool> visited(graph.graphSize() + 1);
	int i;
	double iterations = 0;
	for(i = 1; i < (int)dist.size(); i++){
		if(i != s){
			dist[i] = -1;
		} else dist[i] = 0;
	}
	for(i = 1; i < (int)visited.size(); i++){
		visited[i] = false;
	}
	
	q.insertIntoHeap(s, 0);
	iterations += q.heapUpQty+q.heapDownQty;
	
	while(!q.isEmpty())
	{
	  //cout << "Visiting a vertex. Heap size is " << q.heap.size() << endl;
		v = q.deleteMin();deletemin++;
		//cout << "Removed the vertex. Heap size now is " << q.heap.size() << endl;
		iterations += q.heapUpQty+q.heapDownQty; 

		if(v.getId() == t) break;
		visited[v.getId()] = true;
		//cout << "Before getNeighbourhood " << endl;
		//cout << "Visiting vertex " << v.getId() << endl;
		
		n = graph.getNeighbourhood(v.getId());
		//cout << "There are " << n->size() << " neighbors." << endl;

		//cout << "After getNeighbourhood "<<n << endl;
		for(it = n->begin(); it != n->end(); it++){
		  //cout << "Iterate getNeighbourhood" << endl;

			u = (*it);
			if(!visited[u.getId()]){
				if(dist[u.getId()] == -1){
					dist[u.getId()] = dist[v.getId()] + u.getDistance();
					q.insertIntoHeap(u.getId(), dist[u.getId()]);incert++;
					iterations += q.heapUpQty+q.heapDownQty; 

				}
				else {
					int aux = dist[u.getId()];
					if ((dist[v.getId()] + u.getDistance()) > dist[u.getId()]){
						dist[u.getId()] =	dist[u.getId()];
					}else{
						dist[u.getId()] =   (dist[v.getId()] + u.getDistance());
					}
					
					
					

					q.updateVertex(Vertex(u.getId(), aux), dist[u.getId()]);update++;
					iterations += q.heapUpQty+q.heapDownQty; 

				}
			}
		}
	}
	output <<" "<< iterations  <<" " <<deletemin <<" "<< incert<< " "<< update;
	//cout << "After Djikstra" << endl;
	return dist;
}

int main(int argc, char *argv[])
{
	clock_t tStart = clock();
	int from = atoi(argv[1]);
	int to = atoi(argv[2]);
	//string filename;
	//cin << filename;
	//int start_s=clock();
	//chrono::system_clock::time_point t = chrono::system_clock::now();
 
	assert(argc == 4); 
	Graph graph;
	//graph.loadGraph(filename);
	graph.loadGraph(argv[3]);
	//graph.loadGraph("USA-road-d.NY.gr");
	ofstream output;
	output.open ("Parte3",std::ios::app); 
	output << "\n "<< from <<" "<<to;
	//for (int i = 2; i < 10000; i = i +100)
	//{
		vector<int> dist;
		dist = dijkstra(graph, from,to,output);
		int distance = dist[to];


		if(distance != -1) {
			cout << distance << endl;
			//cout<<"Distance between " <<from <<" and "<< to<<" is "<< distance<< endl;
			output << "Distance between " <<from <<" and "<< to<<" is "<< distance;
			output << "Distance between " <<from <<" and "<< to<<" is "<< distance;
		} else{
			cout << "inf" << endl;
			output << "Distance between " <<from <<" and "<< to<<" is inf";
		}
	//cout << chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now()-t).count() << endl;
	//graph.print();
	//cout << "Distance " <<dist<< endl;
		//int stop_s=clock();
		//cout << "Heap size: "<< i << "time: " << (stop_s-start_s)/double(CLOCKS_PER_SEC)*1000 << endl;
	output << " "<<(double)(clock() - tStart)/CLOCKS_PER_SEC;
	output.close();
	return 0;
}

