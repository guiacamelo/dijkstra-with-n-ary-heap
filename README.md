First assignment for the course inf05016 - Advanced Algorithms, at UFRGS, Brazil, lectured by Marcus Ritt

The assignment have the following objectives:

Implementation of an n-ary heap, and Implementation of Dijkstra using the n-ary heap implemented.

Experimentaly determine the n that results in better performance.

Verify if the real complexity is in acoordance with the theoretical limit

Evaluate the algorithm scalability

The problem that is adressed is the shortest path between two cities in a given graph. The instances are in accordance with the [DIMACS](http://www.dis.uniroma1.it/challenge9/download.shtml) challange format.


In order to compile the project, run make in a linux terminal in the folder you downloaded the files:

$ make

The program recieves 3 parameters, the initial city, the destination city, ang the file containing the graph instance.

For example if we whant to find the path from city 5 to city 50 in the graph grapfInstance.graph(in the root folder for easy execution) you shuld run:

./dijkstra 5 50 graphInstance.graph 
the answer should be 254
 
The professor made available a graph generator(gen.cpp) but to compile boost library is necessary. A script was generated to create instances automaticly with shell bash scriptGeracaoBatch1.sh scriptGeracaoBatch2.sh

In order to run in a automated way a shell bash script was created batch1runTest.sh